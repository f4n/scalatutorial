package starting

//insp: https://github.com/mpilquist/scalaz-talk/blob/master/examples.scala
object BeispielSM {

  class MyS{
    def op(a:String, b:String) = a + b
  }
  
  class MyM{
    
    def zero() = 0
    
    def op(a:Int, b:Int) = a + b 
    
  }
  
  def main(args: Array[String]): Unit = {
    //What is polymorphism    http://eed3si9n.com/learning-scalaz/polymorphism.html
    
    val l=List(1,2,3)
    
    //fold/reduce Operation
    println(l.foldLeft(0){ case(summe,wert) => summe + wert})
    
    val m=new MyM
    println(l.foldLeft(m.zero)(m.op))
    
    println(new MyS().op("abc","def"))

//    import scalaz.std.anyVal._
//    import scalaz.std.option._
//    import scalaz.syntax.std.option._
    
    //println(some(20) |+| none |+| some(22))
    import scalaz.syntax.monoid._
    import scalaz.std.map._
    import scalaz.std.string._
    import scalaz.std.int._
    
    //import scalaz._
    //import Scalaz._

    val map1 = Map(1 ->  "ab", 2 -> "def")
    val map2 = Map(1 -> "c", 3 -> "ghi")
    
    println(map1 |+| map2)
    
    
  }

}