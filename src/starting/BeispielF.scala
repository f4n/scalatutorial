package starting

object BeispielF {

  def main(args: Array[String]): Unit = {
    
    val f1 = (a:Int) => a + 1
    
    val f2 = (a:Int) => a + 1
    
    
    println(f1(1))
    
    println((f1 compose f2)(1))
    
    println((f1 andThen f2)(1))
    
    //Reihenfolge f�r nicht assoziative Operationen!
    
    
  }

}